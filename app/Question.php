<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    /**
     * Get the answers for the question.
     */
    public function answers()
    {
        return $this->hasMany('App\Answer');
    }
    
    /**
     * Get next question id.
     *
     * @return int
     */
    public static function getNextQuestionId($currentQuestionPosition)
    {
        // Next question id
        $nextQuestionId = 0;
        
        // Get ascending questions ordered by position column
        $questions = Question::orderBy('position', 'asc')
            ->get();
            
        for ($i = 0; $i < count($questions); $i++) {
            
            // Check if current question position equals to position in our loop
            if ($questions[$i]->position == $currentQuestionPosition) {
                
                // Check if next question in the loop not empty
                if (!empty($questions[$i+1])) {
                    
                    // Set next question
                    $nextQuestionId = $questions[$i+1]->id;
                }
            }
        }
        
        return $nextQuestionId;
    }
}
