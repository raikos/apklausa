<?php

namespace App\Http\Controllers;

use App\Result;

use Illuminate\Http\Request;

use App\Http\Requests;

class WelcomeController extends Controller
{
    /**
     * Display main page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Check if cookie is set
        if (isset($_COOKIE["question_id"])) {
            
            // Redirect to survey if survey already started
            return redirect('survey');
        }
        
        // Get user has end question
        $hasEndQuestion = Result::hasEndQuestions($request);
        
        // Check if user ended survey in the middle of job
        if ($hasEndQuestion) {
            
            // Redirect user to the notification page
            return redirect('survey/ended');
        } else {
            
            // Get results count
            $resultCount = Result::where('ip_address', $request->ip())
                ->where('finished', 1)
                ->count();
                
            // Check if there is any records by user ip
            if ($resultCount > 0) {
                
                // Redirect user to the result page
                return redirect('survey/result');
            }
        }
        
        // Main page view
        return view('welcome');
    }
}
