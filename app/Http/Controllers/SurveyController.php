<?php

namespace App\Http\Controllers;

use DB;

use App\Question;
use App\Answer;
use App\Result;

use Illuminate\Http\Request;

use App\Http\Requests;

class SurveyController extends Controller
{

    /**
     * Fill the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fill(Request $request)
    {
        // Set our first question id
        $cookieQuestionId = 1;
        
        // Check if cookie question_id is not set
        if (!isset($_COOKIE["question_id"])) {
            
            // Set our question_id cookie
            setcookie("question_id", 1, time()+3600*24);
        } else {
            
            // Load our cookie
            $cookieQuestionId = $_COOKIE["question_id"];
        }
        
        // Get user has end question
        $hasEndQuestion = Result::hasEndQuestions($request);
        
        // Check if user ended survey in the middle of job
        if ($hasEndQuestion) {
            
            // Redirect user to the notification page
            return redirect('survey/ended');
        } else {
            
            // Get results count
            $resultCount = Result::where('ip_address', $request->ip())
                ->where('finished', 1)
                ->count();
                
            // Check if there is any records by user ip
            if ($resultCount > 0) {
                
                // Redirect user to the result page
                return redirect('survey/result');
            }
        }

        // Get question data by question id
        $question = Question::find($cookieQuestionId);
        // Get answers data by question id
        $answers = Question::find($cookieQuestionId)->answers;
        
        // Next question id
        $nextQuestionId = Question::getNextQuestionId($question->position);
        
        // Set boolean variable for checking if post data exists
        $postDataExists = false;
        
        // Check if request has question input data stored
        if ( $request->has($question->short_name) ) {

            // Set post data exists to true
            $postDataExists = true;
            
            // Check if question input type not file
            if ($question->type != "file") {
                
                // Validate all required fields
                $this->validate($request, [
                    $question->short_name => 'required',
                ]);
            }
            
            // Check question type is text
            if ($question->type == "text") {
                
                // Get input field
                $text = $request->input($question->short_name);

                // Create result object
                $result = new Result;

                $result->question_id = $cookieQuestionId;
                $result->answer_id = null;
                $result->ip_address = $request->ip();
                $result->answer = $text;
                $result->image_url = null;
                
                // Store our object data to database
                $result->save();
            } else if ($question->type == "radio") { // Check question type is radio
            
                // Get input field
                $answerId = $request->input($question->short_name);
                // Get database record by answer id
                $answer = Answer::find($answerId);
                
                // Create result object
                $result = new Result;

                $result->question_id = $cookieQuestionId;
                $result->answer_id = $answerId;
                $result->ip_address = $request->ip();
                $result->answer = '';
                $result->image_url = null;
                
                // Store our object data to database
                $result->save();
                
                // Check if answer has end question
                if ($answer->end_question) {
                    // Redirect user to the notification page
                    return redirect('survey/ended');
                }
            } else if ($question->type == "checkbox") { // Check question type is checkbox
            
                // Get input field
                $checkboxes = $request->input($question->short_name);
                // Get all answers records from database
                $answers = Answer::all();
                
                // Set variable for checking end question
                $foundEndQuestion = false;
                
                // Loop through checkboxes
                foreach ($checkboxes as $checkbox => $value) {
                    
                    // Create result object
                    $result = new Result;

                    $result->question_id = $cookieQuestionId;
                    $result->answer_id = $value;
                    $result->ip_address = $request->ip();
                    $result->answer = '';
                    $result->image_url = null;
                    
                    // Store our object data to database
                    $result->save();
                    
                    // Loop through answers
                    foreach ($answers as $answer) {
                        // Ignore not same answer id
                        if ($value != $answer->id) continue;
                        
                        // If answer has end question
                        if ($answer->end_question) {
                            // We've found end question
                            $foundEndQuestion = true;
                        }
                    }
                }
                
                // Check if at least 1 answer found with end question
                if ($foundEndQuestion) {
                    
                    // Redirect user to the notification page
                    return redirect('survey/ended');
                }
            } else if ($question->type == "date") {
                $date = $request->input($question->short_name);
                
                // Create result object
                $result = new Result;

                $result->question_id = $cookieQuestionId;
                $result->answer_id = null;
                $result->ip_address = $request->ip();
                $result->answer = $date;
                $result->image_url = null;
                
                // Store our object data to database
                $result->save();
            }
            
            // Check if we've found next question
            if ($nextQuestionId > 0) {
                
                // Get next question data
                $question = Question::find($nextQuestionId);
                // Get next question's answers data
                $answers = Question::find($nextQuestionId)->answers;
                
                // Store new question phase
                setcookie("question_id", $nextQuestionId, time()+3600*24);
            }
        } else {
   
            // Check if input field type is file
            if ($question->type == "file") {
                
                // Get file instance
                $file = $request->file($question->short_name);

                // Check if uploaded file is valid
                if (!empty($file) && $file->isValid()) {
                    
                    // Validate file, not larger than 500KB
                    $this->validate($request, [
                        $question->short_name => 'max:500',
                    ]);
                    
                    // Move uploaded file
                    $destinationPath = 'myface/';
                    $fileName = strtotime("now").'_'.$file->getClientOriginalName();
                    $file->move($destinationPath,$fileName);
                    
                    // Create result object
                    $result = new Result;

                    $result->question_id = $cookieQuestionId;
                    $result->answer_id = null;
                    $result->ip_address = $request->ip();
                    $result->answer = '';
                    $result->image_url = $destinationPath.$fileName;
                    $result->finished = 1;
                    
                    // Store our object data to database
                    $result->save();
                    
                    // Check if we haven't found next question
                    if ($nextQuestionId == 0) {
                        return redirect('survey/result');
                    }
                } else {
                    // Create result object
                    $result = new Result;

                    $result->question_id = $cookieQuestionId;
                    $result->answer_id = null;
                    $result->ip_address = $request->ip();
                    $result->answer = '';
                    $result->image_url = null;
                    $result->finished = 1;
                    
                    // Store our object data to database
                    $result->save();
                    
                    // Check if we haven't found next question
                    if ($nextQuestionId == 0) {
                        return redirect('survey/result');
                    }
                }
            }
            
            // Check if post validated
            if (!$request->isMethod('post')) {
                // Set post data exists to true
                $postDataExists = true;
            }
        }
        
        // Pass parameters to our survey view
        return view("survey", ['question' => $question, 'answers' => $answers, 'postDataExists' => $postDataExists]);
    }
    
    /**
     * Survey end page.
     *
     * @return \Illuminate\Http\Response
     */
    public function ended(Request $request)
    {
        return view("ended");
    }
    
    /**
     * Survey result page.
     *
     * @return \Illuminate\Http\Response
     */
    public function result(Request $request)
    {
        $results = DB::table('results')
            ->leftJoin('questions', 'questions.id', '=', 'results.question_id')
            ->leftJoin('answers', 'answers.id', '=', 'results.answer_id')
            ->select('results.*', 'questions.name as questionName', 'questions.type', 'answers.name as answerName')
            ->get();
            
        $resultGrouped = array();
            
        foreach ($results as $result) {
            $resultGrouped[$result->question_id]["name"] = $result->questionName;
            $resultGrouped[$result->question_id]["type"] = $result->type;
            $resultGrouped[$result->question_id]["image_url"] = $result->image_url;
            $resultGrouped[$result->question_id]["group"][] = $result;
        }
        
        return view("result", ['results' => $resultGrouped]);
    }
}
