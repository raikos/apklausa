<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Http\Request;

class Result extends Model
{
    /**
     * Get the answer that holds results.
     */
    public function answer()
    {
        return $this->belongsTo('App\Answer');
    }
    
    /**
     * Get user has end question.
     *
     * @return bool
     */
    public static function hasEndQuestions(Request $request)
    {
        // Get results with answer that has end survey option
        $resultsFiltered = Result::where('ip_address', $request->ip())
            ->whereHas('answer', function ($query) {
                $query->where('end_question', 1);
            })->get();
            
        if (count($resultsFiltered) > 0)
            return true;
        
        return false;
    }
}
