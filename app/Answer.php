<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    /**
     * Get the question that holds answers.
     */
    public function question()
    {
        return $this->belongsTo('App\Question');
    }
}
