<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'WelcomeController@index');
Route::any('/survey', 'SurveyController@fill');
Route::any('/survey/ended', 'SurveyController@ended');
Route::any('/survey/result', 'SurveyController@result');
