<form class="form-horizontal" role="form" method="POST" action="{{ url('/survey') }}" enctype="multipart/form-data">
    {!! csrf_field() !!}

    <div class="form-group">
        <div class="col-md-6">
            <input type="file" name="{{ $question->short_name }}">
            
            @if ($errors->has($question->short_name))
                <span class="help-block">
                    <strong>{{ $errors->first($question->short_name) }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="btn btn-primary">Toliau</button>
        </div>
    </div>
</form>