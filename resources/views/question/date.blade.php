<form class="form-horizontal" role="form" method="POST" action="{{ url('/survey') }}">
    {!! csrf_field() !!}

    <div class="form-group">

        <div class="col-md-6">
            <input class="form-control" type="text" id="date" data-format="YYYY-MM-DD" data-template="YYYY MM DD" name="{{ $question->short_name }}" value="2016-01-01">
            
            @if (!$postDataExists)
                <span class="help-block">
                    <strong>@lang('validation.required')</strong>
                </span>
            @endif
            @if ($errors->has($question->short_name))
                <span class="help-block">
                    <strong>{{ $errors->first($question->short_name) }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="btn btn-primary">Toliau</button>
        </div>
    </div>
</form>

<!-- Load Date libraries -->
<script src="{{ url('js/moment.min.js') }}"></script>
<script src="{{ url('js/combodate.js') }}"></script>

<!-- Custom field script -->
<script>
$(function(){
    // Set date formatting
    $('#date').val( moment().format('YYYY-MM-DD') );
    
    // Initialiaze our ComboDate functioning
    $('#date').combodate({
        minYear: 1930,
        maxYear: 2025,
        firstItem: 'none',
        smartDays: true
    });    
});
</script>