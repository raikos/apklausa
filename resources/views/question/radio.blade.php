<form class="form-horizontal" role="form" method="POST" action="{{ url('/survey') }}">
    {!! csrf_field() !!}

    <div class="form-group">
        @foreach ($answers as $answer)
        <div class="radio">
            <label>
                <input type="radio" name="{{ $question->short_name }}" value="{{ $answer->id }}">
                {{ $answer->name }}
            </label>
        </div>
        @endforeach
        
        @if (!$postDataExists)
            <span class="help-block">
                <strong>@lang('validation.required')</strong>
            </span>
        @endif
    </div>
    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="btn btn-primary">Toliau</button>
        </div>
    </div>
</form>