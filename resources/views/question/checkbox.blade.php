<form class="form-horizontal" role="form" method="POST" action="{{ url('/survey') }}">
    {!! csrf_field() !!}

    <div class="form-group">
        @foreach ($answers as $answer)
        <div class="checkbox">
            <label>
                <input class="checkbox_type" data-endquestion="{{ $answer->end_question }}" type="checkbox" name="{{ $question->short_name }}[]" value="{{ $answer->id }}">
                {{ $answer->name }}
            </label>
        </div>
        @endforeach
        
        @if (!$postDataExists)
            <span class="help-block">
                <strong>@lang('validation.required')</strong>
            </span>
        @endif
    </div>
    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="btn btn-primary">Toliau</button>
        </div>
    </div>
</form>
<!-- Custom field script -->
<script>
$(function(){
    
    // Set checkbox check/uncheck event
    $(".checkbox_type").change(function() {
        
        // Checkbox checked
        if(this.checked) {
            
            // Get checkbox answer end question parameter
            var endQuestion = $(this).data("endquestion");
            
            // If end question is set
            if (endQuestion) {
                
                // Uncheck checkboxes
                $('.checkbox_type').prop('checked', false);
                // Set checkboxes disabled
                $(".checkbox_type").prop("disabled", true);
                
                // Set current checkbox checked
                $(this).prop('checked', true);
                // Set current checkbox disabled
                $(this).prop("disabled", false);
            }
        } else { // Checkbox unchecked
        
            // Get checkbox answer end question parameter
            var endQuestion = $(this).data("endquestion");
            
            // If end question is set
            if (endQuestion) {
                // Set checkboxes disabled
                $(".checkbox_type").prop("disabled", false);
            }
        }
    });   
});
</script>