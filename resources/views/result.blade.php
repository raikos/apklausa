@extends('welcome')

@section('content')
    Jūsų užpildyti duomenys:<br />
    @foreach ($results as $result)
        @if ($result['type'] == 'file' && $result['image_url'] === null)
            @continue
        @endif
        <br />
        
        @if ($result['type'] != 'file')
            {{ $result['name'] }}
        @else
            Jūsų nuotrauka:
        @endif
        
        @foreach ($result['group'] as $group)
            @if ($group->type == 'text')
                {{ $group->answer }}
            @elseif ($group->type == 'date')
                {{ $group->answer }}
            @elseif ($group->type == 'radio')
                {{ $group->answerName }}
            @elseif ($group->type == 'checkbox')
                {{ $group->answerName }},
            @elseif ($group->type == 'file' && $group->image_url !== null)
                <img src="{{ url($group->image_url) }}" alt="My Face" style="max-width:200px;max-height:200px;">
            @endif
        @endforeach
    @endforeach
@endsection