<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel 5.3 Apklausa</title>
        
        <!-- Styles -->
        <link href="{{ url('css/app.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ url('css/survey.css') }}" rel="stylesheet" type="text/css">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Load libraries -->
        <script src="{{ url('js/app.js') }}"></script>
    </head>
    <body>
        <div class="outer">
            <div class="middle">
                <div class="container-fluid inner">
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-4">
                            @section('content')
                            <a href="{{ url('/survey') }}" class="btn btn-primary btn-lg btn-block" role="button">Pradėti apklausą!</a>
                            @show
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
