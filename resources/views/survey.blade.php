@extends('welcome')

@section('content')
    {{ $question->name }}
    <br /><br />
    @include('question.'.$question->type, ['question' => $question, 'answers' => $answers, 'postDataExists' => $postDataExists])
@endsection