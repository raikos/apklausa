<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('question_id')->unsigned();
			$table->integer('answer_id')->unsigned()->nullable();
			$table->string('ip_address', 16);
            $table->string('answer');
			$table->string('image_url')->nullable();
            $table->boolean('finished')->nullable();
            $table->timestamps();
        });
		
		Schema::table('results', function($table) {
			$table->foreign('question_id')->references('id')->on('questions');
			$table->foreign('answer_id')->references('id')->on('answers');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('results');
    }
}
