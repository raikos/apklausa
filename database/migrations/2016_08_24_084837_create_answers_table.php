<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('question_id')->unsigned();
			$table->string('name');
			$table->string('short_name');
			$table->boolean('end_question');
			$table->smallInteger('position')->default(1);
            $table->timestamps();
        });
        
		Schema::table('answers', function($table) {
			$table->foreign('question_id')->references('id')->on('questions');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('answers');
    }
}
